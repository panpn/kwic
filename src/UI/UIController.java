package UI;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by choc on 21/8/2015.
 */
public class UIController implements Initializable {

    @FXML //  fx:id="submitButton"
    private Button submitButton; // Value injected by FXMLLoader

    @FXML
    private TextArea input;

    @FXML
    private TextArea wordsToIgnore;

    @FXML
    private TextArea output;

    @FXML
    private ChoiceBox<String> systemType;


    @Override // This method is called by the FXMLLoader when initialization is complete
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        assert submitButton != null : "fx:id=\"myButton\" was not injected: check your FXML file 'simple.fxml'.";
        input.setText("The Day after Tomorrow\nFast and Furious\nMan of Steel");
        wordsToIgnore.setText("is the of and as a after");
        // initialize your logic here: all @FXML variables will have been injected

    }

    @FXML
    public void onSubmit() throws IOException {
        String systemTypeValue = systemType.getValue();
        String result;
        if (systemTypeValue.equals("Abstract Data")) {
            result = ADTDesign.KWIC.execute(input.getText(), wordsToIgnore.getText());
        } else {
            result = SharedStorageDesign.main.KWIC.execute(input.getText(), wordsToIgnore.getText());
        }
        output.setText(result);
    }

}
