package SharedStorageDesign.main;

import SharedStorageDesign.storage.Phrase;
import SharedStorageDesign.storage.Storage;

import java.util.ArrayList;

/**
 * Created by jchiam on 20/8/2015.
 */
public class CircShift {

    private static CircShift instance = null;

    /** CONSTRUCTORS **/
    private CircShift() {}

    /** PUBLIC METHODS **/
    public static CircShift getInstance() {
        if(instance == null) {
            instance = new CircShift();
        }

        return instance;
    }

    /**
     * Execute circular shift on all phrases in storage
     */
    public void run() {

        Storage storage = Storage.getInstance(false);
        ArrayList<Phrase> phrases = new ArrayList<Phrase>();


        while(storage.hasPhrase()) {
            Phrase phrase = storage.readPhrase();

            // proceed with circular shift
            for(int i=0; i<phrase.getLength(); i++) {
                if(!storage.isIgnoreWord(phrase.getFirstWord())) {
                    phrases.add(phrase);
                }

                phrase = shift(phrase);
            }
        }

        for(Phrase p : phrases) {
            storage.addPhrase(p);
        }
    }

    /** PRIVATE METHODS **/
    /**
     * Shifts first word to back of phrase
     * @param phrase to process
     */
    private Phrase shift(Phrase phrase) {
        Phrase newPhrase = new Phrase();
        newPhrase.copy(phrase);

        String word = newPhrase.getFirstWord();
        newPhrase.removeFirstWord();
        newPhrase.addWord(word);

        return newPhrase;
    }
}
