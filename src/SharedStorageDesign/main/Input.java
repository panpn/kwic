package SharedStorageDesign.main;

import SharedStorageDesign.storage.Storage;

import java.io.StringReader;
import java.util.Arrays;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by jchiam on 20/8/2015.
 */
public class Input {

    private static Input instance = null;

    /** CONSTRUCTORS **/
    private Input() {}

    /** PUBLIC METHODS **/
    public static Input getInstance() {
        if(instance == null) {
            instance = new Input();
        }

        return instance;
    }

    /**
     * Initialises input into storage
     * @param files file input
     * @throws IOException if file read error
     */
    public void initialize(String[] files) {

        // read files
        readInputFile(files[0]);
        readIgnoreInputFile(files[1]);
    }

    /**
     * Initialises input into storage
     * @param input
     * @param ignoreWords
     */
    public void initialize(String input, String ignoreWords) throws IOException {

        // read input
        readInput(input);
        readIgnoreInput(ignoreWords);
    }

    /** PRIVATE METHODS **/
    private void readInputFile(String file) {

        Storage storage = Storage.getInstance(false);
        Scanner s = null;

        try {
            s = new Scanner(new BufferedReader(new FileReader(file)));

            while (s.hasNext()) {
                storage.addPhrase(s.nextLine().toLowerCase());
            }
        } catch(IOException e) {
            System.err.println("Input file read error...");
        } finally {
            if(s != null) {
                s.close();
            }
        }
    }

    private void readInput(String input) throws IOException {

        Storage storage = Storage.getInstance(false);
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new StringReader(input));

            String line;
            while((line = reader.readLine()) != null) {
                storage.addPhrase(line.toLowerCase());
            }
        } catch(IOException e) {
            System.err.println("Input read error...");
        } finally {
            if(reader != null) {
                reader.close();
            }
        }
    }

    private void readIgnoreInputFile(String file) {

        Storage storage = Storage.getInstance(false);
        Scanner s = null;

        try {
            s = new Scanner(new BufferedReader(new FileReader(file)));

            while (s.hasNext()) {
                storage.addIgnoreWord(s.next().toLowerCase());
            }
        } catch(IOException e) {
            System.err.println("Input file read error...");
        } finally {
            if(s != null) {
                s.close();
            }
        }
    }

    private void readIgnoreInput(String ignoreWords) throws IOException {

        Storage storage = Storage.getInstance(false);
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new StringReader(ignoreWords));

            String line;
            while((line = reader.readLine()) != null) {
                String words[] = line.split("[^a-zA-Z\\d]+");

                for(int i=0; i<words.length; i++) {
                    storage.addIgnoreWord(words[i]);
                }

                Arrays.fill(words, null);
            }
        } catch(IOException e) {
            System.err.println("Input ignore words read error...");
        } finally {
            if(reader != null) {
                reader.close();
            }
        }
    }
}
