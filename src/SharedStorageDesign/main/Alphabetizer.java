package SharedStorageDesign.main;

import SharedStorageDesign.storage.Phrase;
import SharedStorageDesign.storage.Storage;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by jchiam on 20/8/2015.
 */
public class Alphabetizer {

    private static Alphabetizer instance = null;

    /** CONSTRUCTORS **/
    private Alphabetizer() {}

    /** PUBLIC METHODS **/
    public static Alphabetizer getInstance() {
        if(instance == null) {
            instance = new Alphabetizer();
        }

        return instance;
    }

    /**
     * Sort all phrases in storage
     */
    public void sort() {
        Storage storage = Storage.getInstance(false);
        ArrayList<Phrase> phrases = new ArrayList<Phrase>();

        while(storage.hasPhrase()) {
            phrases.add(storage.readPhrase());
        }

        Collections.sort(phrases);

        for(Phrase p : phrases) {
            storage.addPhrase(p);
        }
    }
}
