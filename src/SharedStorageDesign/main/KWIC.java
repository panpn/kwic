package SharedStorageDesign.main;

import SharedStorageDesign.storage.Storage;

import java.io.IOException;

/**
 * Created by jchiam on 20/8/2015.
 */
public class KWIC {

    public static String execute(String input, String ignoreWords) throws IOException {

        // initialise components
        Storage storage = Storage.getInstance(true);
        Input inputs = Input.getInstance();
        CircShift circShift = CircShift.getInstance();
        Alphabetizer alphabetizer = Alphabetizer.getInstance();

        // run components sequentially
        inputs.initialize(input, ignoreWords);
        circShift.run();
        alphabetizer.sort();

        // send to output
        //storage.printPhrases();
        //storage.printIgnoreWords();
        return storage.outputPhrases();
    }
}
