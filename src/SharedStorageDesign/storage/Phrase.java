package SharedStorageDesign.storage;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by choc on 17/8/2015.
 * Modified by jchiam
 */
public class Phrase implements Comparable<Phrase> {

    private ArrayList<String> line;

    public Phrase() {
        line = new ArrayList<String>();
    }

    public Phrase(String text) {
        line = new ArrayList<String>(Arrays.asList(text.split("\\W")));
    }

    public void addWord(String word) {
        line.add(word);
    }

    public String getWord(int index) {
        return line.get(index);
    }

    public String getFirstWord() {
        return line.get(0);
    }

    public void removeFirstWord() {
        line.remove(0);
    }

    public int getLength() {
        return line.size();
    }

    public void copy(Phrase phrase) {
        line.clear();

        for(int i=0; i<phrase.getLength(); i++) {
            line.add(phrase.getWord(i));
        }
    }

    public void capitalise() {
        Storage storage = Storage.getInstance();

        String wordToCap = line.get(0).toUpperCase();
        line.set(0, wordToCap);
    }

    @Override
    public int compareTo(Phrase otherPhrase) {
        return toString().toLowerCase().compareTo(otherPhrase.toString().toLowerCase());
    }

    @Override
    public String toString() {
        Storage storage = Storage.getInstance();

        String output = new String();

        for(int i=0; i<line.size(); i++) {
            String word = line.get(i);

            output = output.concat(word);
            output = output.concat(" ");
        }

        return output.trim();
    }
}