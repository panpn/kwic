package SharedStorageDesign.storage;

import java.util.ArrayList;

/**
 * Created by jchiam on 20/8/2015.
 */
public class Storage {

    private static Storage instance = null;
    private ArrayList<Phrase> phrases;
    private ArrayList<String> ignoreWords;

    /** CONSTRUCTORS **/
    private Storage() {
        phrases = new ArrayList<Phrase>();
        ignoreWords = new ArrayList<String>();
    }

    /** PUBLIC METHODS **/
    public static Storage getInstance() {
        if(instance == null) {
            instance = new Storage();
        }

        return instance;
    }

    public static Storage getInstance(boolean isNewInstance) {
        if(instance == null || isNewInstance) {
            instance = new Storage();
        }

        return instance;
    }

    /**
     * Add a string as a phrase to storage
     * @param phrase to be added to storage
     */
    public void addPhrase(String phrase) {
        phrases.add(new Phrase(phrase));
    }

    /**
     * Add a phrase to storage
     * @param phrase to be added to storage
     */
    public void addPhrase(Phrase phrase) {
        phrases.add(phrase);
    }

    /**
     * Gets the first phrase in the list of input phrases
     * NOTE: removes first phrase from storage
     * @return the first phrase as a Phrase object
     */
    public Phrase readPhrase() {
        Phrase phrase = phrases.get(0);
        phrases.remove(0);

        return phrase;
    }

    /**
     * Checks if there are phrases in storage
     * @return true if there are
     */
    public boolean hasPhrase() {
        if(phrases.size() != 0) {
            return true;
        }

        return false;
    }

    /**
     * Clears storage of all phrases
     */
    public void clearPhrases() {
        phrases.clear();
    }

    /**
     * Add a string as an ignore word to storage
     * @param word to be added to storage
     */
    public void addIgnoreWord(String word) {
        ignoreWords.add(word);
    }

    /**
     * Checks if word is in list of words to ignore
     * @param word to be checked
     * @return true, false
     */
    public boolean isIgnoreWord(String word) {
        if(ignoreWords.contains(word.toLowerCase())) {
            return true;
        }

        return false;
    }

    /**
     * Prepares a string for output of results
     * @return the output string of results
     */
    public String outputPhrases() {

        StringBuilder sb = new StringBuilder();

        for(Phrase p : phrases) {
            p.capitalise();
            sb.append(p);
            sb.append("\n");
        }

        return sb.toString();
    }

    public void printPhrases() {
        for(Phrase p : phrases) {
            System.out.println(p);
        }
    }

    public void printIgnoreWords() {
        System.out.print(ignoreWords);
    }
}
