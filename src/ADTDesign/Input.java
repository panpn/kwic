package ADTDesign;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Jonathan Chiam on 18/8/2015.
 */
public class Input {

    /**
     * Initialisation for input phrases
     * @param inputText input text
     * @return arraylist of Phrase objects containing the input
     * @throws IOException when file is not found
     */
    public static ArrayList<Phrase> initializeInput(String inputText) {
        ArrayList<Phrase> inputPhrases = new ArrayList<Phrase>();
        Scanner scanner = new Scanner(inputText);
        while (scanner.hasNextLine()) {
            String inputMessage = scanner.nextLine();
            Phrase inputPhrase = new Phrase(inputMessage);
            inputPhrases.add(inputPhrase);
        }
        scanner.close();
        return inputPhrases;
    }

    /**
     * Initialisation for input ignore words
     * @param ignoreInput name of file containing input
     * @return arraylist of String of ignore words
     * @throws IOException when file is not found
     */
    public static ArrayList<String> initializeIgnoreInput(String ignoreInput) {

        ArrayList<String> ignoreWords = new ArrayList<String>();
        Scanner scanner = new Scanner(ignoreInput);
        while (scanner.hasNext()) {
            ignoreWords.add(scanner.next().toLowerCase());
        }
        scanner.close();
        return ignoreWords;
    }

}
