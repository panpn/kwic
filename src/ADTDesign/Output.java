package ADTDesign;

import java.util.ArrayList;

/**
 * Created by choc on 29/8/2015.
 */
public class Output {

    //returns all the formatted phrases as a string
    public static String getOutputString(ArrayList<Phrase> result) {
        StringBuilder sb = new StringBuilder();
        for (Phrase phrase : result) {
            sb.append(getFormattedPhraseString(phrase));
            sb.append(System.getProperty("line.separator"));
        }
        return sb.toString();
    }

    //Capitalises the first word of the phrase
    private static String getFormattedPhraseString(Phrase phrase) {
        StringBuilder sb = new StringBuilder();
        ArrayList<String> phraseText = phrase.getTitle();
        String firstWord = phraseText.get(0);
        sb.append(firstWord.toUpperCase());
        for (int i = 1; i < phraseText.size(); i++) {
            String currentWord = phraseText.get(i);
            sb.append(" ");
            sb.append(currentWord.toLowerCase());
        }
        return sb.toString().trim();
    }
}
