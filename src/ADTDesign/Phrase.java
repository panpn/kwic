package ADTDesign;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by choc on 17/8/2015.
 */
public class Phrase implements Comparable<Phrase> {

    private ArrayList<String> title;

    public Phrase() {
        title = new ArrayList<String>();
    }

    public Phrase(String text) {
        title = new ArrayList<String>(Arrays.asList(text.split(" ")));
    }

    public void addWord(String word) {
        title.add(word);
    }

    public String getWord(int index) {
        return title.get(index);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (title.isEmpty()) {
            return new String();
        } else {
            sb.append(title.get(0));
            for (int i = 1; i < title.size(); i++) {
                String word = title.get(i);
                sb.append(" ");
                sb.append(word);
            }
        }
        return sb.toString();
    }

    public ArrayList<String> getTitle() {
        return title;
    }

    public int getLength() {
        return title.size();
    }


    @Override
    public int compareTo(Phrase otherPhrase) {
        return toString().toLowerCase().compareTo(otherPhrase.toString().toLowerCase());
    }
}
