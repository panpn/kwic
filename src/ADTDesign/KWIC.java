package ADTDesign;

import java.util.ArrayList;

/**
 * Created by Jonathan Chiam on 17/8/2015.
 */
public class KWIC {


    public static String execute(String inputText, String wordsToIgnore) {
        ArrayList<Phrase> inputPhrases = Input.initializeInput(inputText);
        ArrayList<String> inputIgnoreWords = Input.initializeIgnoreInput(wordsToIgnore);
        ArrayList<Phrase> circShiftedPhrases = CircularShift.circShiftAllPhrases(inputPhrases, inputIgnoreWords);
        ArrayList<Phrase> sortedPhrases = Alphabetizer.sortResult(circShiftedPhrases);
        String result = Output.getOutputString(sortedPhrases);
        return result;
    }


}
