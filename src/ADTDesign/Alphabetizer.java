package ADTDesign;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Jonathan Chiam on 18/8/2015.
 */
public class Alphabetizer {

    //returns a sorted list of phrases
    public static ArrayList<Phrase> sortResult(ArrayList<Phrase> result) {
        Collections.sort(result);
        return result;
    }
}
