package ADTDesign;

import java.util.ArrayList;

/**
 * Created by choc on 17/8/2015.
 */
public class CircularShift {

    //returns a list of all the phrases after circular shifting them
    public static ArrayList<Phrase> circShiftAllPhrases(ArrayList<Phrase> phrases, ArrayList<String> excludedWords) {
        ArrayList<Phrase> result = new ArrayList<Phrase>();
        for (Phrase phrase : phrases) {
            result.addAll(circShift(phrase, excludedWords));
        }
        return result;
    }

    //circular shift an individual phrase
    private static ArrayList<Phrase> circShift(Phrase phrase, ArrayList<String> excludedWords) {
        ArrayList<Phrase> result = new ArrayList<Phrase>();
        for (int i = 0; i < phrase.getLength(); i++) {
            if(!excludedWords.contains(phrase.getWord(i).toLowerCase())) {
                result.add(getShiftedPhrase(phrase, i));
            }
        }
        return result;
    }

    //Shifts a phrase around a particular index
    private static Phrase getShiftedPhrase(Phrase original, int index) {
        Phrase newPhrase = new Phrase();
        for (int i = index; i < original.getLength(); i++) {
            newPhrase.addWord(original.getWord(i));
        }
        for (int i = 0 ; i < index; i++) {
            newPhrase.addWord(original.getWord(i));
        }
        return newPhrase;
    }

//    public static void main(String[] args) {
//        ArrayList<String> excludedWord = new ArrayList<String>(Arrays.asList("the", "tomorrow"));
//        Phrase phrase = new Phrase();
//        phrase.addWord("the");
//        phrase.addWord("day");
//        phrase.addWord("after");
//        phrase.addWord("tomorrow");
//        CircularShift shift = new CircularShift();
//        ArrayList<Phrase> result = shift.circShift(phrase,excludedWord);
//
//    }

}
